package Date_2023_03_23;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class Order {
    
    int id;
    String customerName;
    long price;
    Date orderDate;
    boolean confirm;
    String[] items;

       
    public Order(int id, String customerName, long price, Date orderDate, boolean confirm, String[] items) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = orderDate;
        this.confirm = confirm;
        this.items = items;
    }

    public Order(int id, String customerName, long price) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = new Date();
        this.confirm = true;
        this.items = new String[] {"rice", "potato", "apple"};
    }

    public Order(String customerName) {
        this.id = 1;
        this.customerName = customerName;
        this.price = 120000;
        this.orderDate = new Date();
        this.confirm = true;
        this.items = new String[] {"pen", "pencel", "ruler"};
    }

    public Order() {
        this("2");
    }

    public String toString(){
        Locale.setDefault(new Locale("vi", "VN"));
        String pattern = "dd-MMMM-yyyy HH:mm:ss.SSS";
        DateTimeFormatter defautTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        Locale usLocale = Locale.getDefault();
        NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);
        return "Order -- id: " + id 
        + " , customerName: " + customerName 
        + " , price: " + usNumberFormat.format(price) 
        + " , orderDate: " + defautTimeFormatter.format(orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
        + " , confirm: " + confirm 
        + " , items: " + Arrays.toString(items);
    }


}
