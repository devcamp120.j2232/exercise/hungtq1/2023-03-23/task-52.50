import java.util.ArrayList;
import java.util.Date;

import Date_2023_03_23.Order;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");

        ArrayList<Order> arrayList = new ArrayList<>();
            Order order1 = new Order();
            Order order2 = new Order("lan");
            Order order3 = new Order(3, "Loing", 800000);
            Order order4 = new Order(4, "Nam", 750000, new Date(), false, new String[]{"tim", "hong", "cam"});

            arrayList.add(order1);
            arrayList.add(order2);
            arrayList.add(order3);
            arrayList.add(order4);

        for (Order order: arrayList){
            System.out.println(order.toString());
        }
    }
}
